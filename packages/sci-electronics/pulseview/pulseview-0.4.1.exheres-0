# Copyright 2014-2018 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake
require freedesktop-desktop gtk-icon-cache

SUMMARY="Qt based logic analyzer GUI for sigrok"
HOMEPAGE="http://sigrok.org/wiki/PulseView"
DOWNLOADS="http://sigrok.org/download/source/${PN}/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
        app-text/asciidoctor [[ note = [ Automagic dependency for building the manual ] ]]
    build+run:
        dev-libs/boost[>=1.65.1]
        dev-libs/glib:2[>=2.28.0]
        gnome-bindings/glibmm:2.4[>=2.28.0]
        sci-electronics/libsigrok[cxx][>=0.5.1]
        sci-electronics/libsigrokdecode[>=0.5.2]
        x11-libs/qtbase:5[gui]
        x11-libs/qtsvg:5
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-manual-Use-GNUInstallDirs-as-well.patch
)
CMAKE_SRC_CONFIGURE_PARAMS=(
    '-DENABLE_DECODE:BOOL=TRUE'
    '-DENABLE_STACKTRACE:BOOL=TRUE'
    '-DENABLE_TESTS:BOOL=TRUE'
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

