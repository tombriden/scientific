# Copyright 2010 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Qalculate release=v${PV} suffix=tar.gz ]
require alternatives

export_exlib_phases src_prepare src_install

SUMMARY="Modern desktop calculator library"
DESCRIPTION="
Qalculate! is a modern multi-purpose desktop calculator for GNU/Linux. It is
small and simple to use but with much power underneath. Features include
customizable functions, units, arbitrary precision, plotting, and a
user-friendly interface.
"
HOMEPAGE="https://qalculate.github.io/ ${HOMEPAGE}"

LICENCES="GPL-2"
MYOPTIONS="
    exchange-rates [[ description = [ Support for retrieval of exchange rates ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/gmp:=
        dev-libs/icu:=
        dev-libs/libxml2:2.0[>=2.3.8]
        dev-libs/mpfr:=
        sys-libs/readline:=
    run:
        !sci-libs/libqalculate:0[<2.0.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

if ever at_least 3.8.0 ; then
    DEPENDENCIES+="
        build:
            app-doc/doxygen
    "
fi

BUGS_TO="eternaleye@gmail.com"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-icu
    # --with-readline is heavily recommended, but unfortunately
    # it automagically checks for various libs unless the extra
    # libs are _all_ disabled unconditionally
    --with-readline=only
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'exchange-rates libcurl'
)

libqalculate_src_prepare() {
    default

    edo intltoolize --force --automake

    cat <<EOF >> po/POTFILES.in
data/currencies.xml.in
data/datasets.xml.in
data/elements.xml.in
data/functions.xml.in
data/planets.xml.in
data/units.xml.in
data/variables.xml.in
src/defs2doc.cc
EOF
}

libqalculate_src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/bin/qalc               qalc-${SLOT}
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/${PN}.a            ${PN}-${SLOT}.a
        /usr/${host}/lib/${PN}.la           ${PN}-${SLOT}.la
        /usr/${host}/lib/${PN}.so           ${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    for linguas in fr nl sv zh_CN  ; do
        other_alternatives+=(
            /usr/share/locale/${linguas}/LC_MESSAGES/${PN}.mo ${PN}-${SLOT}.mo
        )
    done
    other_alternatives+=( /usr/share/qalculate /usr/share/qalculate-${SLOT} )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
}

